import requests
import mysql.connector
from bs4 import BeautifulSoup

def affectedList(serials, field_notice_id):
    serial_string = ",".join(serials)
    page = requests.get("http://serialnumbervalidation.com/" + field_notice_id + "/cgi-bin/index.cgi?act=process&checklist=" + serial_string)
    soup = BeautifulSoup(page.content, 'html.parser')
    tables = soup.findChildren('table')
    body = tables[-2].findChildren('tbody')
    rows = body[0].findChildren('tr')
    affected = []
    for r in rows:
        cells = r.findChildren('td')
        if str(cells[1].string) == "Affected":
            affected.append([str(cells[0].string), field_notice_id])
    return affected
       
def open_mysql_connection(): # This is called in write_to_mysql and is used to connect to the database
    test_db = mysql.connector.connect(
        host="192.168.100.180",
        user="testlab",
        passwd="speakFRIEND9(9",
        database="p3systems"
    )
    return test_db
    
def write_to_mysql_ASA(): # Writes to mySQL database and inserts searched information each time a set of perameters are filled
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%ASA%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '64228'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_
    
def write_to_mysql_ISR(): # Writes to mySQL database and inserts searched information each time a set of perameters are filled
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%ISR4%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '64253'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_
    
def write_to_mysql_N9K(): # Writes to mySQL database and inserts searched information each time a set of perameters are filled
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%N9K%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '64251'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_
    
def write_to_mysql_AIR(): 
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%AIR-%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '70143'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_
    
def write_to_mysql_GLC(): 
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%GLC-SX%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '70120'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_
    
def write_to_mysql_UCS(): 
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%UCS%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '63499'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_

def write_to_mysql_CTS(): 
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%CTS%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '64217'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_

def write_to_mysql_ASA5506(): 
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%ASA5506%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '64079'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_

def write_to_mysql_ASR9001(): 
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%ASR-9001%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '63646'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_

def write_to_mysql_C2960(): 
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%C2960%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '63340'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_
  
def write_to_mysql_C3560(): 
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%C3560%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '63251'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_

def write_to_mysql_CISCO39(): 
    current_database = open_mysql_connection()
    mycursor = current_database.cursor()
    sql = "SELECT * FROM p3systems.whs_item WHERE part_number LIKE '%CISCO39%'"
    list_ = []
    mycursor.execute(sql)
    records = mycursor.fetchall()
    for row in records:
        id_ = row[0]
        part_num = row[3]
        serial_num = row[4]
        fn_id = '63723a'
        temp = affectedList(serial_num,fn_id)
        if temp:
            list_.append(temp)
    current_database.commit()
    mycursor.close()
    return list_

BigList = []
ASA = write_to_mysql_ASA()
BigList.append(ASA)
ISR = write_to_mysql_ISR()
BigList.append(ISR)
AIR = write_to_mysql_AIR()
BigList.append(AIR)
GLC = write_to_mysql_GLC()
BigList.append(GLC)
UCS = write_to_mysql_UCS()
BigList.append(UCS)
CTS = write_to_mysql_CTS()
BigList.append(CTS)
ASA5506 = write_to_mysql_ASA5506()
BigList.append(ASA5506)
ASR9001 = write_to_mysql_ASR9001()
BigList.append(ASR9001)
C2960 = write_to_mysql_C2960()
BigList.append(C2960)
C3560 = write_to_mysql_C3560()
BigList.append(C3560)
CISCO39 = write_to_mysql_CISCO39()
BigList.append(CISCO39)
print(BigList)